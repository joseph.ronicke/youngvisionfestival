module.exports = {
  i18n: {
    defaultLocale: "de",
    locales: ["de", "en"],
  },
  images: {
    domains: ["media.graphcms.com"],
    deviceSizes: [384, 640, 784, 1024, 1280, 1536, 1872],
  },
};
