import React, { useState } from "react";
import { useI18n } from "next-rosetta";
import { Locale } from "../i18n";
import { toast } from "react-hot-toast";
import axios from "axios";

function ContactForm() {
  const { t } = useI18n<Locale>();
  const [email, setEmail] = useState("");

  function NotifyButton() {
    return (
      <div className="mt-4 sm:mt-0 sm:ml-3">
        <button
          type="submit"
          className="block w-full rounded-md border border-transparent px-5 py-3 bg-teal-600 text-base font-bold text-white shadow hover:bg-teal-700 focus:outline-none focus:ring-2 focus:ring-rose-500 focus:ring-offset-2 sm:px-10"
        >
          {t("notifyMe")}
        </button>
      </div>
    );
  }

  return (
    <form
      action="#"
      className="mt-12 sm:max-w-lg sm:w-full sm:flex"
      onSubmit={async (event) => {
        event.preventDefault();
        await axios
          .post("/api/notify", { email })
          .then((res) => {
            if (res.status < 400) {
              toast.success(t("willNotifyYou"));
              setEmail("");
            } else {
              return Promise.reject();
            }
          })
          .catch((e) => {
            console.error(e);
            toast.error(t("somethingWentWrong"));
          });
      }}
    >
      <div className="min-w-0 flex-1">
        <label htmlFor="hero_email" className="sr-only">
          Email address
        </label>
        <input
          id="hero_email"
          type="email"
          value={email}
          onChange={(event) => {
            setEmail(event.target.value);
          }}
          className="block w-full border border-gray-300 rounded-md px-5 py-3 text-base text-gray-900 placeholder-gray-500 shadow-sm focus:border-rose-500 focus:ring-rose-500"
          placeholder={t("enterEmail")}
        />
      </div>
      <NotifyButton />
    </form>
  );
}

export const HeroSection: React.FC = () => {
  const { t } = useI18n<Locale>();

  return (
    <div className="overflow-hidden sm:pt-12 lg:relative lg:py-48">
      <div className="mx-auto max-w-md px-4 sm:max-w-3xl sm:px-6 lg:px-8 lg:max-w-7xl lg:grid lg:grid-cols-2 lg:gap-24">
        <div>
          <div className="lg:mt-20">
            <div className="mt-6 sm:max-w-xl">
              <h1 className="text-4xl font-moontype text-gray-900 tracking-tight sm:text-5xl">
                YOUNGVISION FESTIVAL 2021{" "}
                <span className="text-3xl text-gray-500">
                  - Alles ist Jetzt
                </span>
              </h1>
              <p className="mt-2 text-xl text-gray-500">13. - 22. August</p>
              <p className="mt-6 text-lg text-justify text-gray-600">
                {t("greetings")}
              </p>
            </div>
            <ContactForm />
          </div>
        </div>
      </div>
      <div className="sm:mx-auto sm:max-w-3xl sm:px-6">
        <div className="py-12 sm:relative sm:mt-12 sm:py-16 lg:absolute lg:inset-y-0 lg:right-0 lg:w-1/2">
          <div className="hidden sm:block">
            <div className="absolute inset-y-0 left-1/2 w-screen bg-teal-50 rounded-l-3xl lg:left-80 lg:right-0 lg:w-full" />
          </div>
          <div className="relative aspect-h-9 aspect-w-16 pl-4 -mr-40 sm:mx-auto sm:max-w-3xl sm:px-0 lg:max-w-none lg:h-full lg:pl-12">
            <iframe
              className="w-full rounded-md shadow-xl ring-1 ring-black ring-opacity-5 lg:h-full"
              width="1280"
              height="720"
              src="https://www.youtube.com/embed/rE_hb7AVGeY"
              title="YouTube video player"
              frameBorder="0"
              allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen
            />
          </div>
        </div>
      </div>
    </div>
  );
};
