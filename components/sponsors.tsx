import React from "react";
import Image from "next/image";
import { ValuesType } from "utility-types";
import * as sdk from "../lib/youngvision-sdk";
import { useI18n } from "next-rosetta";
import { Locale } from "../i18n";

type Sponsors = sdk.AllSponsorsQuery["sponsors"];
export type Sponsor = ValuesType<Sponsors>;

function notNull<TValue>(value: TValue | null | undefined): value is TValue {
  return value != null;
}

const Sponsors: React.FC<{ sponsors?: Sponsor[] }> = ({ sponsors }) => {
  const { t } = useI18n<Locale>();

  return (
    <section className="bg-white">
      <h1 className="mx-3.5 text-gray-900 font-moontype text-4xl">
        {t("ourSponsors")}
      </h1>
      <div className="flex flex-row overflow-scroll">
        {sponsors?.map((s) => {
          const Wrapper = ({
            children,
          }: {
            children?: JSX.Element | string | null;
          }) =>
            s.url != null ? (
              <a
                href={s.url}
                target="_blank"
                rel="noreferrer"
                className="m-3.5 border-2"
              >
                {children}
              </a>
            ) : (
              <div className="m-3.5 border-2">children</div>
            );

          const logo = s.logo;

          if (notNull(logo)) {
            const width =
              notNull(logo.width) && notNull(logo.height)
                ? (logo.width / logo.height) * 150
                : 150;

            return (
              <Wrapper key={s.id}>
                <Image
                  layout="fixed"
                  height={150}
                  width={width}
                  alt={s.name ?? ""}
                  src={s.logo?.url ?? "https://via.placeholder.com/150"}
                />
              </Wrapper>
            );
          } else {
            return <Wrapper>{s.name}</Wrapper>;
          }
        })}
      </div>
    </section>
  );
};

export default Sponsors;
