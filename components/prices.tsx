import React from "react";
import { AllTicketsQuery } from "../lib/youngvision-sdk";
import { ValuesType } from "utility-types";
import { useI18n } from "next-rosetta";
import { Locale } from "../i18n";

type TicketType = ValuesType<AllTicketsQuery["tickets"]>;

type PricesProps = {
  onBuy: (ticketName: TicketType) => void;
};

const BuyButton: React.FC<{ onClick: () => void }> = ({ onClick }) => {
  const { t } = useI18n<Locale>();
  return (
    <div className="mt-8">
      <div className="rounded-lg shadow-md">
        <a
          onClick={onClick}
          href="#"
          className="block w-full text-center rounded-lg border border-transparent bg-white px-6 py-3 text-base font-medium text-teal-600 hover:bg-gray-50"
          aria-describedby="tier-hobby"
        >
          {t("buy")}
        </a>
      </div>
    </div>
  );
};

const StandardTicket: React.FC<
  PricesProps & { index: number; ticket: TicketType }
> = ({ onBuy, index, ticket }) => {
  const { name, price, description, available } = ticket;
  if (available === 0) {
    return null;
  }
  let col;
  switch (index) {
    case 0:
      col = "lg:col-start-1 lg:col-end-3";
      break;
    case 1:
      col = "lg:col-start-4 lg:col-end-6";
      break;
    case 2:
      col = "lg:col-start-6 lg:col-end-8";
      break;
    default:
      // dirty fall back
      return null;
  }
  return (
    <div
      className={`mt-10 mx-auto max-w-md lg:m-0 lg:max-w-none lg:row-start-2 lg:row-end-3 ${col}`}
    >
      <div className="h-full flex flex-col rounded-lg shadow-lg overflow-hidden">
        <div className="flex-1 flex flex-col">
          <div className="bg-white px-6 py-10">
            <div>
              <h3
                className="text-center text-2xl font-medium text-gray-900"
                id="tier-scale"
              >
                {name}{" "}
                <span className="text-gray-500 text-lg">
                  {available != null ? `(${available} left)` : ""}
                </span>
              </h3>
              <div className="mt-4 flex items-center justify-center">
                <span className="px-3 flex items-start text-6xl tracking-tight text-gray-900">
                  <span className="mt-2 mr-2 text-4xl font-medium">€</span>
                  <span className="font-extrabold">{price}</span>
                </span>
              </div>
            </div>
          </div>
          <div className="flex-1 flex flex-col justify-between border-t-2 border-gray-100 p-6 bg-gray-50 sm:p-10 lg:p-6 xl:p-10">
            <p className="text-center text-teal-900 text-lg">{description}</p>
            <BuyButton onClick={() => onBuy(ticket)} />
          </div>
        </div>
      </div>
    </div>
  );
};

const Prices: React.FC<PricesProps & { tickets: AllTicketsQuery }> = ({
  onBuy,
  tickets,
}) => {
  return (
    <div className="bg-cyan-900">
      <div className="pt-12 px-4 sm:px-6 lg:px-8 lg:pt-20">
        <div className="text-center">
          <h2 className="text-lg leading-6 font-semibold text-gray-300 uppercase tracking-wider">
            Tickets
          </h2>
          <p className="mt- text-3xl font-extrabold text-white sm:text-4xl lg:text-5xl">
            Das passende Ticket für dich, egal wer du bist
          </p>
        </div>
      </div>
      <div className="mt-16 bg-white pb-12 lg:mt-20 lg:pb-20">
        <div className="relative z-0">
          <div className="absolute inset-0 h-5/6 bg-cyan-900 lg:h-2/3" />
          <div className="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
            <div className="relative lg:grid lg:grid-cols-7">
              {tickets.tickets.map((t, i) => (
                <StandardTicket index={i} key={t.id} onBuy={onBuy} ticket={t} />
              ))}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Prices;
