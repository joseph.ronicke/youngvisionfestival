import React from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import { useI18n } from "next-rosetta";
import { Locale } from "../i18n";

const LINKS = {
  Home: "/",
  Tickets: "/tickets",
  Program: "/program",
};

export const NavBar: React.FC = () => {
  const { t } = useI18n<Locale>();
  const { pathname } = useRouter();

  const entries = [];
  let page: keyof typeof LINKS;
  for (page in LINKS) {
    const link = LINKS[page];
    const className = pathname == link ? "text-gray-900" : "text-gray-500";
    const element = (
      <Link key={page} href={link}>
        <a className={"text-base font-medium hover:text-gray-900 " + className}>
          {t("menu")[page]}
        </a>
      </Link>
    );
    entries.push(element);
  }
  return (
    <div className="relative bg-white">
      <div className="max-w-7xl mx-auto px-4 sm:px-6">
        <div className="flex justify-center items-center border-b-2 border-gray-100 py-6 md:justify-start md:space-x-10">
          <nav className="flex space-x-10">{entries}</nav>
        </div>
      </div>
    </div>
  );
};
