import { OrderDetails } from "../pages/api/buyTypes";

export interface Locale {
  locale: string;
  title: string;
  metaDescription: string;
  greetings: string;
  notifyMe: string;
  willNotifyYou: string;
  somethingWentWrong: string;
  enterEmail: string;
  ourSponsors: string;
  menu: {
    Home: string;
    Tickets: string;
    Program: string;
    Teams: string;
  };
  program: {
    welcome: string;
  };
  orderDetails: { [P in keyof Omit<OrderDetails, "disclaimer">]: string } & {
    disclaimer: { minor: string; adult: string };
  };
  buy: string;
}
