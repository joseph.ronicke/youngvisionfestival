import { Locale } from ".";

export const table: Locale = {
  locale: "Deutsch",
  title: "YoungVision Festival 2021",
  metaDescription: "YoungVision Festival 2021 - vom 13. bis 22. August",
  greetings: `Wie schön, dass Du hier bist! Wir können es kaum erwarten, dich im Sommer kennenzulernen - oder wiederzusehen. Alle Infos zu Ort, Programm und Gestaltung des Festivals, sowie dem Ticketverkauf erfährst Du schon ganz bald auf dieser Seite. Sollen wir dir Bescheid sagen, wenn es Neuigkeiten gibt?`,
  notifyMe: "Ja bitte",
  willNotifyYou: "Du hörst von uns!",
  somethingWentWrong: "Das hat nicht geklappt! Bitte versuche es noch einmal",
  enterEmail: "Gib deine Email ein",
  ourSponsors: "Unsere Sponsoren",
  menu: {
    Home: "Home",
    Tickets: "Tickets",
    Program: "Programm",
    Teams: "Teams",
  },
  program: {
    welcome: `In diesem Programm erforschen wir gemeinsam das Sein im Jetzt, die verschiedenen Aspekte davon und wie es ist, mit dem was dich jetzt beschäftigt, im Jetzt zu sein.
Denn Alles ist Jetzt, alles was du denkst, fühlst und wahrnimmst. Das passiert alles immer jetzt. Klingt verrückt, und auch irgendwie nicht. Das letzte Jahr hat die verschiedensten Themen hochgeholt und wir wollen Räume schaffen, in dem alles einen Platz finden und gesehen, gehört, gefühlt werden kann. Und wir meinen wirklich alles: aktuelle Weltthemen, persönliche Prozesse, Beziehung, Familie, Konflikte, Freude…
Wir forschen natürlich nicht alleine, sondern haben das Programm ausgeschmückt mit verschiedenen Referent*innen. Wer alles dabei sein wird? Und was die so machen werden? Das erfährst du fast gleich. Wir sitzen an den Feinheiten und sobald das Programm steht, werden wir es hier veröffentlichen.`,
  },
  orderDetails: {
    ticket: "Ticket",
    name: "Voller Name",
    age: "Alter in Jahren (bei Festivalbeginn)",
    email: "Email",
    country: "Land",
    streetAddress: "Straße & Hausnummer",
    city: "Stadt",
    zipCode: "PLZ",
    disclaimer: {
      adult: "Ich werde den Haftungsauschluss unterschreiben",
      minor:
        "Ein*e Erziehungsberechtigte*r wird den Haftungsauschluss unterschreiben",
    },
  },
  buy: "Kaufen",
};
