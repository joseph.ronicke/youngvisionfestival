import { Locale } from ".";

export const table: Locale = {
  locale: "English",
  title: "YoungVision Festival 2021",
  metaDescription: "YoungVision Festival 2021 - from 13. to 22. August",
  greetings:
    "How great to have you here! We can't wait to meet you - or see you again - this summer. You'll find out all about the festival's location, program and ticket sales on this page very soon. Should we let you know when there are updates?",
  notifyMe: "Yes please",
  willNotifyYou: "Will notify you soon!",
  somethingWentWrong: "That did not work. Please try again",
  enterEmail: "Enter your email",
  ourSponsors: "Our sponsors",
  menu: {
    Home: "Home",
    Tickets: "Tickets",
    Program: "Program",
    Teams: "Teams",
  },
  program: {
    welcome: `In our programme we explore together being in the here and now, different aspects of it and what it is like to be in the here and now with whatever you are dealing with in the present moment.
Because everything is now, everything you think, feel and perceive. It's all always happening now. Sounds crazy, and somehow it doesn't. The last year has brought up all kinds of topics and we want to create spaces where everything can find a place and be seen, heard and felt. And we really mean everything: current world issues, personal processes, relationship, family, conflicts, joy…
Of course, we are not researching alone, but have embellished the programme with various speakers. Who will be there? And what will they be doing? You'll find out very soon. We are working on the details and as soon as the programme is ready, we will publish it here.`,
  },
  orderDetails: {
    ticket: "Ticket",
    name: "Full name",
    age: "Age in years (at festival start)",
    email: "Email",
    country: "Country",
    streetAddress: "Street address",
    city: "City",
    zipCode: "Zip Code",
    disclaimer: {
      minor: "A guardian will sign the disclaimer",
      adult: "I'll sign the disclaimer",
    },
  },
  buy: "Buy",
};
