import React, { useState } from "react";
import { GetStaticProps } from "next";
import { useMachine } from "@xstate/react";
import { Machine } from "xstate";
import { Locale } from "../i18n";
import Prices from "../components/prices";
import { ErrorMessage, Field, FieldProps, Form, Formik } from "formik";
import getSDK from "../lib/sdk";
import { AllTicketsQuery } from "../lib/youngvision-sdk";
import { OrderDetails, schema } from "./api/buyTypes";
import { useI18n } from "next-rosetta";
import axios from "axios";
import { toast, Toaster } from "react-hot-toast";

const checkoutMachine = Machine({
  id: "checkout",
  initial: "covid",
  states: {
    covid: { on: { AGREE: "choosing" } },
    choosing: { on: { SELECT: "enterDetails" } },
    enterDetails: { on: { BUY: "bought" } },
    bought: {},
  },
});

const CovidInfo: React.FC<{ onAgree: () => void }> = ({ onAgree }) => {
  return (
    <div className="bg-teal-700">
      <div className="max-w-7xl mx-auto py-16 px-4 sm:py-24 sm:px-6 lg:px-8">
        <h2 className="text-3xl font-extrabold text-white">
          Unser COVID 19 Konzept
        </h2>
        <div className="mt-6 border-t border-teal-300 border-opacity-25 pt-10 text-white text-justify text-lg">
          <p>
            Alles ist Jetzt - auch <b>Corona</b>. Wir als Festival-Team haben
            lange hin und her überlegt und sind letztendlich zu dem Entschluss
            gekommen, das Festival trotz der momentanen Pandemie-Situation
            stattfinden zu lassen. Nachdem es letztes Jahr aufgrund der vielen
            Einschränkungen kein Life-Festival gab, wollen wir es dieses Jahr
            versuchen in dieser nach wie vor schwierigen Situation ein
            einmaliges YoungVision Festival in real life für dich zu kreieren.
          </p>
          <p className="mt-6">
            In enger Zusammenarbeit mit den örtlichen Behörden sind wir
            weiterhin dabei ein dementsprechend aktuelles Hygiene-Konzept zu
            entwerfen. Natürlich können wir nicht in die Zukunft schauen, und
            haben keine Kontrolle darüber wie die Inzidenz Fälle sich in den
            nächsten Monaten entwickeln werden. Hier sind unsere aktuell
            wichtigsten Bestimmungen, wann und wie das YoungVision Festival 2021
            stattfinden kann:
          </p>
          <dl className="space-y-10 md:space-y-0 md:grid md:grid-cols-2 md:grid-rows-2 md:gap-x-8 md:gap-y-12 mt-10">
            <div>
              <dt className="text-lg leading-6 font-medium text-white">
                Das Festival wird bei einer 7-Tage Inzidenz von unter 35
                stattfinden.
              </dt>
              <dd className="mt-2 text-base text-teal-200">
                Wichtig: hier sind wir abhängig von der Inzidenz des Landkreises
                Bautzen, wo das Festival stattfinden wird, sowie von den im
                August festgelegten Maßnahmen. Sollte die Inzidenz im August
                weiterhin über 35 sein und es eine permanente Maskenpflicht in
                Innenräumen geben, wird das Festival nicht live stattfinden.
                Dies kann noch bis zu zwei Wochen vorher festgelegt werden.{" "}
                <b>
                  Im Falle einer Stornierung aufgrund von Corona werden dir die
                  gesamten Ticketkosten rückerstattet.
                </b>
              </dd>
            </div>
            <div>
              <dt className="text-lg leading-6 font-medium text-white">
                Abstandsregel von 1,5 Meter auf dem Festivalgelände
              </dt>
              <dd className="mt-2 text-base text-teal-200">
                <p>
                  Spätestens jetzt wirst du dich bestimmt fragen:{" "}
                  <i>
                    “Was soll ich denn bitte auf dem YoungVision Festival, wenn
                    ich zu jedem Menschen 1,5 Meter Abstand halten muss?”
                  </i>
                </p>
                <p className="mt-2">
                  Aktuell haben wir eine Möglichkeit gefunden, dass auch näherer
                  Kontakt zwischen den Festivalteilnehmenden stattfinden kann.
                  Und zwar wird es für jeden Teilnehmenden ein Kontakt-Tagebuch
                  geben. Hier sollte jede*r festhalten mit wem diese Person am
                  Tag Körperkontakt hatte. Dies ermöglicht uns eine einfache und
                  sichere Rückverfolgung von möglichen Infektionsketten. Die
                  Anzahl an Personen zu denen du näheren Kontakt hast, sollte
                  jedoch nicht mehr als 15 Personen pro Tag sein.
                </p>
              </dd>
            </div>
            <div>
              <dt className="text-lg leading-6 font-medium text-white">
                Negatives Testergebnis
              </dt>
              <dd className="mt-2 text-base text-teal-200">
                Jede*r Teilnehmende kann nur mit einer schriftlichen Bestätigung
                eines negativen Antigen Schnelltests, der nicht älter als 48
                Stunden sein darf, das Festivalgelände betreten. Dieser kann
                bereits kostenfrei in diversen Corona-Testzentren/Apotheken
                durchgeführt werden.
              </dd>
            </div>
            <div>
              <dt className="text-lg leading-6 font-medium text-white">
                Masken
              </dt>
              <dd className="mt-2 text-base text-teal-200">
                Dennoch wird es in den Innenräumen sowie bei Anhäufung von
                mehreren Menschen verpflichtend sein, die Abstandsregeln sowie
                ggf. das Tragen einer medizinischen Mund-Nase-Bedeckung
                einzuhalten wie z.B. bei der Essensausgabe.
              </dd>
            </div>
            <div>
              <dt className="text-lg leading-6 font-medium text-white">
                Tage vor dem Festival
              </dt>
              <dd className="mt-2 text-base text-teal-200">
                Mit der Bestellung eines Tickets stimmst du zu, dass du 14 Tage
                vor dem Festival keine internationalen Reisen oder in stark
                betroffene Gebiete unternehmen wirst. Des weiteren bist du dazu
                angehalten in eine einwöchige Quarantäne vor dem Festival zu
                gehen und während der Anreise und des Wartens auf Einlass keinen
                Körperkontakt zu haben und nur mit Maske zu interagieren.
              </dd>
            </div>
          </dl>
          <p className="mt-12">
            Diese Leitlinie ist eine Momentaufnahme von März 2021, diese kann
            sich natürlich bis August noch ändern. Wir werden dich
            dementsprechend immer wieder auf dem Laufenden halten, sollte es
            Neuigkeiten oder Änderungen geben.
          </p>
          <p className="mt-4">
            Auch wenn die Situation gerade schwierig erscheint, bleiben wir
            optimistisch und geben unser Bestes ein wundervoll einmaliges
            Festival ganz im YoungVision Spirit stattfinden zu lassen.
          </p>
          <p className="mt-4">
            Nur wenn du diesen Maßnahmen zustimmst und dich an diese halten
            wirst darfst du ein Ticket kaufen.
            <br />
            <b>
              Solltest du vor Ort gegen diese verstoßen, werden wir dich ohne
              Erstattung nach Hause schicken.
            </b>
          </p>
          <p className="mt-4">
            Vor Ort wirst du hierzu einen Haftungsauschluss unterschreiben
          </p>
        </div>
        <div className="mt-8">
          <div className="rounded-lg shadow-md">
            <a
              onClick={() => onAgree()}
              href="#"
              className="block w-full text-center rounded-lg border border-transparent bg-white px-6 py-3 text-base font-bold text-teal-600 hover:bg-gray-50"
              aria-describedby="tier-hobby"
            >
              Einverstanden
            </a>
          </div>
        </div>
      </div>
    </div>
  );
};

type InputProps = React.DetailedHTMLProps<
  React.InputHTMLAttributes<HTMLInputElement>,
  HTMLInputElement
>;

const FormField: React.FC<InputProps & { name: keyof OrderDetails }> = ({
  name,
  autoComplete,
  type = "text",
  ...inputProps
}) => {
  const { t } = useI18n<Locale>();

  const errorId = `${name}-error`;
  const renderField = ({
    field, // { name, value, onChange, onBlur }
    meta: { touched, error },
  }: FieldProps) => {
    const isError = touched && error;
    const inputClasses = isError
      ? "focus:ring-red-500 focus:border-red-500 border-red-300 text-red-900 placeholder-red-300"
      : "focus:ring-teal-500 focus:border-teal-500 border-gray-300";
    return (
      <div className="sm:grid sm:grid-cols-3 sm:gap-4 sm:items-start sm:border-t sm:border-gray-200 sm:pt-5">
        <label
          htmlFor={name}
          className="block text-sm font-medium text-gray-700 sm:mt-px sm:pt-2"
        >
          {t("orderDetails")[name]}
        </label>
        <div className="mt-1 relative sm:mt-0 sm:col-span-2">
          <input
            autoComplete={autoComplete}
            type={type}
            {...field}
            {...inputProps}
            className={`max-w-lg block w-full shadow-sm rounded-md sm:max-w-xs sm:text-sm ${inputClasses}`}
            aria-describedby={errorId}
          />
          {isError && (
            <div className="absolute inset-y-0 right-0 pr-3 flex items-center pointer-events-none">
              <svg
                className="h-5 w-5 text-red-500"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 20 20"
                fill="currentColor"
                aria-hidden="true"
              >
                <path
                  fillRule="evenodd"
                  d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7 4a1 1 0 11-2 0 1 1 0 012 0zm-1-9a1 1 0 00-1 1v4a1 1 0 102 0V6a1 1 0 00-1-1z"
                  clipRule="evenodd"
                />
              </svg>
            </div>
          )}
        </div>
        <p className="mt-2 h-4 text-sm text-red-600" id={errorId}>
          <ErrorMessage name={name} />
        </p>
      </div>
    );
  };
  return <Field name={name}>{renderField}</Field>;
};

const Disclaimer: React.FC<{ age: number | null }> = ({ age }) => {
  const { t } = useI18n<Locale>();

  const renderField = ({ field, meta: { touched, error } }: FieldProps) => {
    const { name } = field;
    const isError = touched && error;
    const errorId = `${name}-error`;
    const inputClasses = isError
      ? "focus:ring-red-500 focus:border-red-500 border-red-300 text-red-900 placeholder-red-300"
      : "focus:ring-teal-500 focus:border-teal-500 border-gray-300 text-teal-600";
    const disclaimer =
      age != null && age < 18
        ? t("orderDetails").disclaimer.minor
        : t("orderDetails").disclaimer.adult;

    return (
      <div className="sm:grid sm:grid-cols-3 sm:gap-4 sm:items-start sm:border-t sm:border-gray-200 sm:pt-5">
        <label
          htmlFor={name}
          className="block text-sm font-medium text-gray-700 sm:mt-px sm:pt-2"
        >
          {disclaimer}
          <br />
          <a
            href="/YoungVision Festival 2021 Haftungsausschluss.pdf"
            className="text-indigo-800"
            download
          >
            Diesen Haftungsauschluss
          </a>
        </label>
        <div className="mt-1 relative sm:mt-0 sm:col-span-2">
          <input
            {...field}
            className={`h-4 w-4 rounded ${inputClasses}`}
            type="checkbox"
          />
          {isError && (
            <div className="absolute inset-y-0 right-0 pr-3 flex items-center pointer-events-none">
              <svg
                className="h-5 w-5 text-red-500"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 20 20"
                fill="currentColor"
                aria-hidden="true"
              >
                <path
                  fillRule="evenodd"
                  d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7 4a1 1 0 11-2 0 1 1 0 012 0zm-1-9a1 1 0 00-1 1v4a1 1 0 102 0V6a1 1 0 00-1-1z"
                  clipRule="evenodd"
                />
              </svg>
            </div>
          )}
        </div>
        <p className="mt-2 h-4 text-sm text-red-600" id={errorId}>
          <ErrorMessage name={name} />
        </p>
      </div>
    );
  };

  return (
    <Field type="checkbox" name="disclaimer">
      {renderField}
    </Field>
  );
};

const EnterDetails: React.FC<{
  onSubmit: () => void;
  ticket: string;
}> = ({ onSubmit, ticket }) => {
  const { t } = useI18n<Locale>();

  return (
    <Formik
      initialValues={{
        ticket,
        name: "",
        age: null,
        email: "",
        country: "Deutschland",
        streetAddress: "",
        city: "",
        zipCode: "",
        disclaimer: false,
      }}
      validationSchema={schema}
      onSubmit={async (values, { setSubmitting }) => {
        await axios
          .post("/api/buy", values)
          .then(() => onSubmit())
          .catch(() => toast.error(t("somethingWentWrong")));
        setSubmitting(false);
      }}
    >
      {({ values: { age }, isSubmitting, submitCount }) => (
        <div className="max-w-7xl mx-auto py-12 px-4 sm:px-6 lg:px-8">
          <Form className="space-y-8 divide-y divide-gray-200">
            <div className="space-y-8 divide-y divide-gray-200 sm:space-y-5">
              <div>
                <h3 className="text-lg leading-6 font-medium text-gray-900">
                  Persönliche Informationen
                </h3>
                <span className="text-gray-600 text-sm">Ticketinhaber*in</span>
              </div>
              <div className="space-y-6 sm:space-y-5">
                {submitCount >= 3 ? (
                  <div>
                    Contact us at{" "}
                    <a href="mailto:tickets@youngvisionfestival.de">
                      tickets@youngvisionfestival.de
                    </a>{" "}
                    if you are having problems
                  </div>
                ) : null}
                <FormField name="name" autoComplete="name" />
                <FormField name="age" type="number" min="12" max="99" />
                <FormField name="email" type="email" autoComplete="email" />
                <FormField name="country" autoComplete="country-name" />
                <FormField name="streetAddress" autoComplete="street-address" />
                <FormField name="city" autoComplete="city" />
                <FormField name="zipCode" autoComplete="postal-code" />
                <Disclaimer age={age} />
              </div>
              <div className="pt-5">
                <div className="flex justify-end">
                  <button
                    type="submit"
                    disabled={isSubmitting}
                    className="ml-3 inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-teal-600 hover:bg-teal-700 focus:outline-none disabled:ring-0 focus:ring-2 focus:ring-offset-2 focus:ring-teal-500 disabled:bg-gray-500"
                  >
                    Zahlungspflichtig bestellen
                  </button>
                </div>
              </div>
            </div>
          </Form>
        </div>
      )}
    </Formik>
  );
};

function Success() {
  return (
    <div className="bg-white">
      <div className="max-w-7xl mx-auto text-center py-12 px-4 sm:px-6 lg:py-16 lg:px-8">
        <h2 className="text-3xl font-extrabold tracking-tight text-gray-900 sm:text-4xl">
          <span className="block">Danke!</span>
          <span className="block">Wir freuen uns schon sehr auf dich.</span>
        </h2>
        <p>
          Du wirst in wenigen Minuten eine Email mit Bezahldetails von uns
          erhalten.
        </p>
      </div>
    </div>
  );
}

type Props = {
  tickets: AllTicketsQuery;
};

const Tickets: React.FC<Props> = ({ tickets }) => {
  const [state, send] = useMachine(checkoutMachine);
  const [ticket, setTicket] = useState<string>("");

  const CurrentPage = () => {
    switch (state.value) {
      case "choosing":
        return (
          <Prices
            tickets={tickets}
            onBuy={(ticket) => {
              setTicket(ticket.id);
              send("SELECT");
            }}
          />
        );
      case "covid":
        return <CovidInfo onAgree={() => send("AGREE")} />;
      case "enterDetails":
        return <EnterDetails ticket={ticket} onSubmit={() => send("BUY")} />;
      case "bought":
        return <Success />;
      default:
        return <div>Something went wrong, please start again.</div>;
    }
  };

  return (
    <div className="bg-white">
      <CurrentPage />
      {/* Footer 4-column with newsletter and localization dark */}
      <div className="text-gray-600 px-3 pb-4 text-lg text-center">
        Schreib uns bei Fragen{" "}
        <a
          className="text-indigo-600"
          href="mailto:tickets@youngvisionfestival.de"
        >
          eine Mail
        </a>
      </div>
      <Toaster />
    </div>
  );
};

export const getStaticProps: GetStaticProps<
  Props & {
    table: Locale;
  }
> = async (context) => {
  const locale = context.locale ?? context.defaultLocale;
  const { table = {} } = await import(`../i18n/${locale}`); // Import locale
  const sdk = getSDK({ locale: locale ?? "de" });
  const tickets = await sdk.allTickets();
  return { props: { table, tickets } };
};

export default Tickets;
