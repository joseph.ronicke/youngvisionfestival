import { NextApiRequest, NextApiResponse } from "next";
import getSDK from "../../lib/sdk";
import { schema } from "./buyTypes";
import { createTransport } from "nodemailer";

const GRAPHCMS_TOKEN = process.env.GRAPHCMS_TOKEN_MUTATE;
if (GRAPHCMS_TOKEN == null || GRAPHCMS_TOKEN.length === 0) {
  console.error("$GRAPHCMS_TOKEN_MUTATE is not set");
}

const resolve = async (
  req: NextApiRequest,
  res: NextApiResponse
): Promise<void> => {
  const { body, method } = req;
  console.log(schema.isValidSync(body));
  const SDK = getSDK({ token: GRAPHCMS_TOKEN });
  if (method === "POST") {
    await schema
      .validate(body, { strict: false })
      .then(
        async ({
          name,
          age,
          email,
          country,
          streetAddress,
          city,
          zipCode,
          ticket,
        }) => {
          const address = `${streetAddress}\n${zipCode} ${city}\n${country}`;
          const { ticket: dataTicket } = await SDK.ticket({ id: ticket });
          let price = null;
          if (dataTicket != null) {
            const { available } = dataTicket;
            if (available != null && available <= 0) {
              throw new Error("No tickets available anymore");
            }
            const { createTicketOrder } = await SDK.orderTicket({
              name,
              age,
              email,
              address,
              ticket,
            });
            price = createTicketOrder?.ticket?.price;
            if (available != null) {
              const updatedAvailable = available - 1;
              await SDK.decrementTicket({
                id: ticket,
                updatedAvailable,
              });
              console.log(
                `Decreased available ${ticket} from ${available} to ${updatedAvailable}.`
              );
            }
          }
          return { name, email, price };
        }
      )
      .then(async ({ email, name, price }) => {
        const transporter = createTransport({
          host: "mail.privateemail.com",
          port: 465,
          secure: true, // true for 465, false for other ports
          auth: {
            user: "tickets@youngvisionfestival.de", // generated ethereal user
            pass: "e,%s]GZp,=;U3yZ-1P", // generated ethereal password
          },
        });

        return transporter.sendMail({
          from: '"YoungVision" <tickets@youngvisionfestival.de>',
          to: email,
          bcc: "tickets@youngvisionfestival.de",
          subject: "YoungVision Festival 2021 Ticket", // Subject line
          text: `Hallo ${name}!
Deine Ticketbestellung ist angekommen und wir freuen uns bereits jetzt schon auf dich!
Bitte überweise gleich ${price} Euro an uns, um sie abzuschliessen.

Verwendungszweck: ${name}
YoungVision e.V.
GLS Bank
IBAN: DE 65430609671154728000
BIC: GENO DE M 1 GLS
`,
        });
      })
      .then((mail) => {
        console.log("Message sent: %s", mail.messageId);
      })
      .then(() => res.status(200).json({}))
      .catch(({ errors }) => res.status(400).json({ errors }));
  } else {
    res.setHeader("Allow", ["POST"]);
    res.status(405).end(`Method ${method} Not Allowed`);
  }
};
export default resolve;
