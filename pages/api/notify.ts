import { NextApiRequest, NextApiResponse } from "next";
import getSDK from "../../lib/sdk";

const GRAPHCMS_TOKEN = process.env.GRAPHCMS_TOKEN_MUTATE;
if (GRAPHCMS_TOKEN == null || GRAPHCMS_TOKEN.length === 0) {
  console.error("$GRAPHCMS_TOKEN is not set");
}

const resolve = async (
  req: NextApiRequest,
  res: NextApiResponse
): Promise<void> => {
  const { body, method } = req;
  const SDK = getSDK({ token: GRAPHCMS_TOKEN });

  if (method === "POST") {
    const email = body.email;
    if (email == null) {
      res.status(400).json({ missing: "email" });
    } else {
      await SDK.addContact({ email }).catch((e) => console.error(e));
      res.status(202).json({});
    }
  } else {
    res.setHeader("Allow", ["POST"]);
    res.status(405).end(`Method ${method} Not Allowed`);
  }
};

export default resolve;
