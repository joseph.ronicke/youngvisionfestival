import * as yup from "yup";
import { SchemaOf } from "yup";

export type OrderDetails = {
  ticket: string;
  name: string;
  age: number;
  email: string;
  country: string;
  streetAddress: string;
  city: string;
  zipCode: string;
  disclaimer: true;
};
export const schema: SchemaOf<OrderDetails> = yup.object().shape({
  ticket: yup.string().required(),
  name: yup.string().required(),
  age: yup.number().required().integer().min(12),
  email: yup.string().required(),
  country: yup.string().required(),
  streetAddress: yup.string().required(),
  city: yup.string().required(),
  zipCode: yup.string().required(),
  disclaimer: yup.mixed().oneOf([true]).required(),
});
