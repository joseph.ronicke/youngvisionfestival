import React from "react";
import { GetStaticProps } from "next";
import { AllTeamsQuery } from "../lib/youngvision-sdk";
import { ValuesType } from "utility-types";
import Image from "next/image";
import getSDK from "../lib/sdk";
import { I18nProps } from "next-rosetta";
import { Locale } from "../i18n";

type TeamsType = AllTeamsQuery["teams"];
type TeamType = ValuesType<TeamsType>;
type TeamMemberType = ValuesType<TeamType["teamMembers"]>;

const TeamMember: React.FC<{ member: TeamMemberType }> = ({ member }) => {
  return (
    <li>
      <div className="flex items-center space-x-4 lg:space-x-6">
        <Image
          layout="fixed"
          width={80}
          height={80}
          className="w-16 h-16 rounded-full lg:w-20 lg:h-20"
          src={member?.image?.url ?? "/Portrait_Placeholder.png"}
          alt={`image of ${member.name}`}
          objectFit="cover"
          sizes="384px"
        />
        <div className="font-medium text-lg leading-6 space-y-1">
          <h3>{member.name}</h3>
          <p className="text-indigo-600">{member.role}</p>
        </div>
      </div>
    </li>
  );
};

const Team: React.FC<{ team: TeamType; index: number }> = ({ team, index }) => {
  const children = [
    <div key="name" className="space-y-5 sm:space-y-4">
      <h2 className="text-3xl font-extrabold tracking-tight sm:text-4xl">
        {team.name}
      </h2>
      <p className="text-xl text-gray-500">{team.description}</p>
    </div>,
    <div key="members" className="lg:col-span-2">
      <ul className="space-y-12 sm:grid sm:grid-cols-2 sm:gap-12 sm:space-y-0 lg:gap-x-8">
        {team.teamMembers.map((m) => (
          <TeamMember key={m.id} member={m} />
        ))}
      </ul>
    </div>,
  ];
  if (index % 2 === 1) {
    children.reverse();
  }
  return (
    <div className="mx-auto py-12 px-4 max-w-7xl sm:px-6 lg:px-8 lg:py-24">
      <div className="grid grid-cols-1 gap-12 lg:grid-cols-3 lg:gap-8">
        <div className="space-y-5 sm:space-y-4">
          <h2 className="text-3xl font-extrabold tracking-tight sm:text-4xl">
            {team.name}
          </h2>
          <p className="text-xl text-gray-500">{team.description}</p>
        </div>
        <div className="lg:col-span-2">
          <ul className="space-y-12 sm:grid sm:grid-cols-2 sm:gap-12 sm:space-y-0 lg:gap-x-8">
            {team.teamMembers.map((m) => (
              <TeamMember key={m.id} member={m} />
            ))}
          </ul>
        </div>
      </div>
    </div>
  );
};

const Teams: React.FC<{ teams: TeamsType }> = ({ teams }) => {
  const teamComponent =
    teams.length === 0 ? (
      <div className="max-w-7xl mx-auto px-4 sm:px-6 m-8">
        <h1 className="text-6xl font-regular">
          Coming <span className="font-bold">Soon</span>.
        </h1>
      </div>
    ) : (
      teams.map((t, i) => <Team key={t.id} team={t} index={i} />)
    );
  return <div className="bg-white">{teamComponent}</div>;
};

export const getStaticProps: GetStaticProps<
  { teams: TeamsType } & I18nProps<Locale>
> = async (context) => {
  const locale = context.locale ?? context.defaultLocale;
  const { table = {} } = await import(`../i18n/${locale}`); // Import locale
  const SDK = getSDK({ locale: locale ?? "de" });
  const query = await SDK.allTeams();
  return { props: { table, teams: query.teams } };
};

export default Teams;
