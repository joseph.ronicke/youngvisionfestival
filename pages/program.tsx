import React from "react";
import { GetStaticProps } from "next";
import { I18nProps, useI18n } from "next-rosetta";
import { Locale } from "../i18n";

const Program: React.FC = () => {
  const { t } = useI18n<Locale>();

  return (
    <div className="max-w-7xl mx-auto px-4 sm:px-6 m-8">
      {t("program")
        .welcome.split("\n")
        .map((l) => (
          <p key={l} className="lg:text-xl mt-5 font-regular">
            {l}
          </p>
        ))}
    </div>
  );
};

export const getStaticProps: GetStaticProps<I18nProps<Locale>> = async (
  context
) => {
  const locale = context.locale ?? context.defaultLocale;
  const { table = {} } = await import(`../i18n/${locale}`); // Import locale
  return { props: { table } };
};

export default Program;
