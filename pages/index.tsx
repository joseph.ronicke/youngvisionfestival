import React from "react";
import Head from "next/head";
import { GetStaticProps } from "next";
import Footer from "../components/footer";
import { HeroSection } from "../components/hero";
import { useI18n } from "next-rosetta";
import { Locale } from "../i18n";
import { Toaster } from "react-hot-toast";
import { Testimonials } from "../components/testimonials";
import getSDK from "../lib/sdk";
import {
  AllSponsorsQuery,
  AllTestimonialsQuery,
  ShortInfoTextsQuery,
} from "../lib/youngvision-sdk";
import Sponsors from "../components/sponsors";
import ShortInfos from "../components/ShortInfos";

type Props = {
  testimonials: AllTestimonialsQuery;
  sponsors: AllSponsorsQuery;
  shortInfos: ShortInfoTextsQuery;
};

const Home: React.FC<Props> = ({ shortInfos, testimonials, sponsors }) => {
  const { t } = useI18n<Locale>();

  return (
    <div>
      <Head>
        <title>{t("title")}</title>
        <link rel="icon" href="/favicon.ico" />
        <meta name="description" content={t("metaDescription")} />
      </Head>
      <div className="bg-white">
        <main>
          <HeroSection />
          <ShortInfos shortInfoTexts={shortInfos.shortInfoTexts} />
          <Testimonials testimonials={testimonials.testimonials} />
        </main>
        <Sponsors sponsors={sponsors.sponsors} />
        <Footer />
      </div>
      <Toaster />
    </div>
  );
};

export const getStaticProps: GetStaticProps<
  Props & {
    table: Locale;
  }
> = async (context) => {
  const locale = context.locale || context.defaultLocale;
  const { table = {} } = await import(`../i18n/${locale}`); // Import locale
  const sdk = getSDK({ locale: locale ?? "de" });
  const testimonials = await sdk.allTestimonials();
  const sponsors = await sdk.allSponsors();
  const shortInfos = await sdk.shortInfoTexts();
  return { props: { table, testimonials, sponsors, shortInfos } }; // Passed to `/pages/_app.tsx`
};

export default Home;
