import { GraphQLClient } from "graphql-request";
import * as SDK from "./youngvision-sdk";

interface SDKOptions {
  locale?: string;
  token?: string;
}
type SDKFactory = (locale: SDKOptions) => SDK.Sdk;

const getSDK: SDKFactory = ({ locale, token }) => {
  const headers = new Headers({
    "gcms-locales": locale ?? "de",
  });
  const authToken = token ?? process.env.GRAPHCMS_TOKEN;
  if (authToken) {
    headers.set("Authorization", `BEARER ${authToken}`);
  }
  // add fallback locale because can't init an array in constructor:
  // does not work :> new Headers({"gcms-locales": ["de", "en"]})
  headers.append("gcms-locales", "en");

  return SDK.getSdk(
    new GraphQLClient(
      "https://api-eu-central-1.graphcms.com/v2/ckkjt5hqkrufc01wahpiw5bon/master",
      {
        headers,
      }
    )
  );
};

export default getSDK;
